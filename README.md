# SLUD-XIX-Material-Grafico

En este repositorio se manejaran los editables y entregables de los materiales graficos para el SLUD XIX.


## Entregables

Se ubican los archivos que estan listos para publicar o hacer uso se ellos.

## Editables

Se ubican los archivos editables de diseño.

## Referencias_diseño

Se ubican la presentación de las bases para los diseños: Paleta, logo, elementos, tipografia...

La idea es mantener una uniformidad de todos los entregables. Es posible modificar las bases sin ningun problema, si se desean generalizar, los editables de las bases se encuentran en ***Editables***.

### Drive

Para archivos demasiados grandes se recomienda subirlos al Drive: https://drive.google.com/drive/u/0/folders/1lrmWNhnUoqOKgJnx8gTXvCXu2vqBlGv7 

# Material SLUD 2021
Repositotio año pasado: https://gitlab.com/GLUD/slud-xviii-disenos
Animaciones año pasado: https://drive.google.com/drive/folders/1TnjN5r8kiTvF_s-lpR04qNMsR7o7-f71?usp=sharing
